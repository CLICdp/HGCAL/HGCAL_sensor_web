# HexWeb

[![Build Status](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_web/badges/master/pipeline.svg)](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_web/commits/master)

This page contains:

* __Geometries__ : it collects the geometries of the HGCal sensors analyzed in the lab with the respective numbering scheme.

* __Sensors analysis__ : it includes some IV/CV analysis done on different HGCal sensor batches.

* __Sensors list__ : it collects automatically produced default plots for HGCal sensors.

* __Clean Room Monitor__ : it displays automatically produced plots of temperature, pressure, humidity and number of particles in the clean room for the last 24 hours and 15 days.
The code and instructions to run the monitor can be found on the [github folder](https://github.com/twentycrooks/clean_room_monitor/).

The deployed website can be found [here](https://HexWeb.web.cern.ch/HexWeb).

All commits to the master branch of this repository are automatically deployed to the EOS space of the project and thus directly reflect on the published website.
If no new commit is added to the master branch, the website rebuilds and deploys itself in regular intervals. 

## Dependencies

The website is generated from markdown files using the [hugo](https://gohugo.io/) compiler written in Go. In order to install Hugo on your machine, pick the appropriate package or binary from [the latest release](https://github.com/gohugoio/hugo/releases).

## Commands
The following commands might help adding content and testing the website generation locally before committing and deployment:

* __Start a local test server__

    ```
    hugo server -D
    ```
    The server will listen on the address defined in the `config.toml` file, e.g. http://localhost:1313/.
    The additional command line argument `-D` also enables all documents currently in draft mode.
* __Add a new page__

    In order to add a new static page, run
    ```
    hugo new page/pagetitle.md
    ```
    and a new markdown file with the appropriate header will be created and the `pages` menu will appear. The new page can be found in `content/page/pagetitle.md`.
    The draft status is set to `false` by default, change it to `true` if the page is not ready to be published yet.

## Setup

* __Local setup__

    To avoid symlinks and copy some datafiles to a local path
    ```shell
    mkdir Results
    ./utils/copy_test_directories.sh
    ```

* __Preparing the host directory__

    Create the symlink to the directory. If you have EOS mounted and access to the directory `utils/set_symlink.sh` will do. Otherwhise
    ```shell
    ssh hgsensor@lxplus
    ln -s /eos/user/h/hgsensor/HGCAL_test_results/Results/ /eos/user/h/hgsensor/www/Results
    ```

* __Create sensor list webpages__

    Create the sensor list webpages locally with `./create_sensorlist_local.py`. For deployment of the website `./create_sensorlist.py` is used.

* __Create sensor analysis webpages__

    Create the sensor analysis webpages locally with `./create_sensoranalysis.py`. The folder `DATA_PATH` must contain the plots that you want to display - they will be copied in the `static` folder. The target folder will be `HUGO_ANALYSIS_DIR` in `content`. This part is not running in deployment.

## Deployment
Simply push to the `master` branch of this repository to get things published. It should be noted that only documents with `draft = false` will be included in the published website.

## Access
Access to the website is restricted to the e-groups listed in `.htaccess`.


