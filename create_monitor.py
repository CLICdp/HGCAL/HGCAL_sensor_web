#!/usr/bin/python


import argparse
import sys
import os
import shutil
import fileinput
import datetime
import time

from src import ssh_file_walker
from shutil import copytree

DATA_PATH = '/home/hgsensor/Applications/clean_room_monitor/'
EOS_PATH = '/eos/user/h/hgsensor/HGCAL_test_results/CleanRoomMonitor/'
LOCAL_PATH = 'Monitor_data/'

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--eosPass", default=None, help='Password to acces ' + EOS_PATH)
parser.add_argument("--eosUser", default='hgsensor', help='Username to acces ' + EOS_PATH)
parser.add_argument("--monitorPass", default=None, help='Password to acces ' + DATA_PATH)
parser.add_argument("--monitorUser", default='hgsensor', help='Username to acces ' + DATA_PATH)
parser.add_argument("--monitorServer", default='pchglog01', help='Server to acces ' + DATA_PATH)
args = parser.parse_args()

directory = DATA_PATH + 'data/'
eos_directory = EOS_PATH + 'data/'
local_dir = LOCAL_PATH 

if not os.path.exists(local_dir):
    os.mkdir(local_dir)

print(">>> Accessing to the monitor system in the cleaning room")

copy_dir = ssh_file_walker.copydir(args.monitorUser, args.monitorPass, args.monitorServer)
copy_dir(directory, local_dir)

print(">>> Copying data files from the monitor system to eos")

uploadOnlyNotExisting = True
upload_dir = ssh_file_walker.uploaddir(args.eosUser, args.eosPass)
#the forth parameter is an exception file that is NOT uploaded - only for display purposes
upload_dir(local_dir, eos_directory, uploadOnlyNotExisting, 'biweekly_plot.png')

#Copying today's monitor in the website page
today = datetime.date.today()
today_str = str(today).replace("-", "_")

print('>>> Copying today\'s monitor (' + today_str + ') in the website page')

plotcurrentname = local_dir + today_str + ".png"
mainDir = 'static/'
dataSubDir = 'images/monitor/'
plotfinalname = mainDir + dataSubDir + "today_plot.png"
if os.path.isfile(plotcurrentname):
    bashCommand = "cp " + plotcurrentname + " " + plotfinalname
    os.system(bashCommand)
    print(bashCommand)
else:
    yesterday = datetime.date.fromordinal(datetime.date.today().toordinal()-1)
    yesterday_str = str(yesterday).replace("-", "_")
    print('>>> Today\'s does not exist. Copying yesterday\'s monitor (' + yesterday_str + ') in the website page')
    plotcurrentname = local_dir + yesterday_str + ".png"
    if os.path.isfile(plotcurrentname):
        bashCommand = "cp " + plotcurrentname + " " + plotfinalname
        os.system(bashCommand)
        print(bashCommand)

print('>>> Copying biweekly monitor in the website page')

plotcurrentname = local_dir + "biweekly_plot.png"
plotfinalname = mainDir + dataSubDir + "biweekly_plot.png"
if os.path.isfile(plotcurrentname):
    bashCommand = "cp " + plotcurrentname + " " + plotfinalname
    os.system(bashCommand)
    print(bashCommand)

