


def print_file_header(myfile, title):
    print('<html>', file=myfile)
    print('<head>', file=myfile)
    print('<title>{0}</title>'.format(title), file=myfile)
    print('<link rel = "stylesheet"', file=myfile)
    print('  type = "text/css"', file=myfile)
    print('  href = "../styles.css" />', file=myfile)
    print('</head>', file=myfile)
    print('<body>', file=myfile)
    print('<h1>{0}</h1>'.format(title), file=myfile)


def print_file_footer(myfile):
    print('</body>', file=myfile)
    print('</html>', file=myfile)


def print_pdf(myfile, figurePath, caption=""):
    print('<object width="400" height="400" data="{0}"></object>'.format(figurePath), file=myfile)


def print_jpg(myfile, figurePath, caption=''):
    print('<figure>', file=myfile)
    print('<img src="{0}" '
          'alt="alternate text" '
          'width="width" '
          'height="height" '
          '/>'.format(figurePath), file=myfile)
    print('<figcaption><b>{0}</b></figcaption></figure>'.format(caption), file=myfile)


def print_centered_text(myfile, text):
    print('<div class="centered">', file=myfile)
    print(text, file=myfile)
    print('</div>', file=myfile)
