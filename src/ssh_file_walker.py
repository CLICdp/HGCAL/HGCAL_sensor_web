# from __future__ import print_function  # breaks sftp client

import collections
import os
import stat
import shutil
import errno

from shutil import copytree

import paramiko


SFTP_URL   =  'lxplus.cern.ch'

ssh = None
sftp = None

def init_ssh(username, password, sftpname = ""):
    global ssh
    global sftp
    if ssh:
        ssh.close()
        #return sftp
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy( paramiko.AutoAddPolicy() )
    if not sftpname :
        sftpname = SFTP_URL
    ssh.connect(sftpname, username=username, password=password)
    sftp = ssh.open_sftp()
    return sftp

def subdirs(file, files):
	subDirs = [f[len(file):] for f in files if f.startswith(file + '/')]
	return subDirs


def recursive_sftp(sftp, path='./', files=None):
    if files is None:
        files = collections.defaultdict(list)
    # loop over list of SFTPAttributes (files with modes)
    for attr in sftp.listdir_attr(path):

        if stat.S_ISDIR(attr.st_mode):
            # If the file is a directory, recurse it
            recursive_sftp(sftp, os.path.join(path, attr.filename), files)
        else:
            #  if the file is a file, add it to our dict
            filename = str(attr.filename)
            files[str(path)].append(filename)

    return files

def walk(username, password):
	def walk_pw(dataPath):
		directory = dataPath
		print(('Scanning directory', directory))
		sftp = init_ssh(username=username, password=password)
		files = recursive_sftp(sftp, path=directory)
		dirs = sorted(files.keys())
		dirStructure = list([(x, subdirs(x, dirs), files[x]) for x in dirs])
		return dirStructure
	return walk_pw

def listdir(username, password, sftpname = ""):
    def listdir_pw(dataPath):
        directory = dataPath
        print(('Listing directory', directory))
        sftp = init_ssh(username=username, password=password, sftpname=sftpname)
        attempts = 0
        while attempts < 3:
            try:
                files = sftp.listdir(directory)
                break
            except IOError as e:
                attempts += 1
                print(("{0}. try resulted in IOError".format(attempts + 1), e))
        files = list(map(str, files))
        return files
    return listdir_pw

def copydir(username, password, sftpname = ""):
    def copydir_pw(dataPath, dstPath):
        srcPath = dataPath
        print(('Copying directory ' + srcPath + ' in directory ' + dstPath))
        sftp = init_ssh(username=username, password=password, sftpname=sftpname)
        download_files(sftp, srcPath, dstPath)
        ssh.close()
        return
    return copydir_pw

def uploaddir(username, password, sftpname = ""):
        def uploaddir_pw(dataPath, dstPath, onlyNotExisting = False, exceptionFile = ''):
                srcPath = dataPath
                print(('Uploading directory ' + srcPath + ' in directory ' + dstPath))
                sftp = init_ssh(username=username, password=password, sftpname=sftpname)
                upload_files(sftp, srcPath, dstPath, onlyNotExisting, exceptionFile)
                return
        return uploaddir_pw

def download_files(sftp_client, remote_dir, local_dir):
    if not exists_remote(sftp_client, remote_dir):
        print("Remote folder do not exist - Fix it.")
        return

    if not os.path.exists(local_dir):
        os.mkdir(local_dir)

    for filename in sftp_client.listdir(remote_dir):
        if stat.S_ISDIR(sftp_client.stat(remote_dir + filename).st_mode):
            # uses '/' path delimiter for remote server
            download_files(sftp_client, remote_dir + filename + '/', os.path.join(local_dir, filename))
        elif '.pdf' or '.jpg' in filename:
            if not os.path.isfile(os.path.join(local_dir, filename)):
                print(filename)
                sftp_client.get(remote_dir + filename, os.path.join(local_dir, filename))

def upload_files(sftp_client, local_dir, remote_dir, onlyNotExisting, exceptionFile):
    if not exists_remote(sftp_client, remote_dir):
        print("Remote folder do not exist - Fix it.")
        return

    for filename in os.listdir(local_dir):

        if not onlyNotExisting :
            print((remote_dir + filename + ' upload.'))
            sftp_client.put(local_dir + filename, remote_dir + filename)
        else :
            if filename != exceptionFile:
                try:
                    sftp.stat(remote_dir + filename)
                    print((remote_dir + filename + ' already exists.'))
                except IOError:
                    sftp_client.put(local_dir + filename, remote_dir + filename)
                    print((remote_dir + filename + ' uploaded with exception.'))
            else:
                print((remote_dir + filename + ' skipped.'))
                

def exists_remote(sftp_client, path):
    try:
        sftp_client.stat(path)
    except IOError as e:
        if e.errno == errno.ENOENT:
            return False
        raise
    else:
        return True


    
