

import os
import re
import shutil

from shutil import copytree
from src import ssh_file_walker

EOS_PREFIX = '/eos/user/h/hgsensor/HGCAL_test_results/'


def walk_dirs(dataPath, eosPass=None, eosUser=None, verbose=0):
    if eosPass:
        print("This is the password: Won't tell you")
        list_dir = ssh_file_walker.listdir(eosUser, eosPass)
        walk_dir = ssh_file_walker.walk(eosUser, eosPass)
        directory =  EOS_PREFIX + dataPath
    else:
        list_dir = os.listdir
        walk_dir = os.walk
        directory = dataPath

    # read top level dirs
    dataDirs = list_dir(directory)

    print('Found', len(dataDirs), 'dataDirs')
    if verbose:
        print(dataDirs, '\n')

    # derive categories
    dataCategories = dataDirs
    dataCategories = [re.sub('\_Needle$', '', s) for s in dataCategories]
    dataCategories = [re.sub('\_Open$', '', s) for s in dataCategories]
    dataCategories = [re.sub('\_Short$', '', s) for s in dataCategories]
    dataCategories = [s.rstrip('1234567890') for s in dataCategories]
    dataCategories = sorted(set(dataCategories))
    print('Found', len(dataCategories), 'dataCategories')
    if verbose:
        print(dataCategories, '\n')

    # get entire list of subdirs
    dirStructure = list(walk_dir(directory))

    dataSubDirContent = []
    for struct in dirStructure:
        if struct[0] == dataPath:
            continue
        #after cernbox update, some system folders were created - ignore them
        if ".sys.v" in struct[0]:
            continue
        print('struct[0]', struct[0])
        thisDir = struct[0][len(directory):]
        thisDirName = thisDir.replace('/', '_')
        thisDirFiles = list([dataPath + thisDir + '/'+ x for x in struct[2]])
        print('directory', directory)
        print('Appending thisDir')
        print(thisDir)
        print('Appending thisDirName')
        print(thisDirName)
        print('Appending thisDirFiles')
        print(thisDirFiles)
        dataSubDirContent.append([thisDir, thisDirName, thisDirFiles])


    return dataDirs, dataCategories, dataSubDirContent

def copy_dirs(dataPath, dstPath, eosPass=None, eosUser=None ):
    if eosPass:
        print("copy_dirs: This is the password: Won't tell you")
        copy_dir = ssh_file_walker.copydir(eosUser, eosPass)
        scrPath =  EOS_PREFIX + dataPath
        copy_dir(scrPath, dstPath)
    else:
        print("The copy of the folders is local. It is does not need the password.")
        shutil.copytree(dataPath, dstPath)

    return os.path.isdir(dstPath)
