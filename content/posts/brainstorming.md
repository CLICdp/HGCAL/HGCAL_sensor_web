---
title: "Brainstorming"
date: 2018-07-15T12:18:56+02:00
draft: true
weight: 5000
---

List of stuff to fix:

* Fix search function (almost)
* Elog for posts
* List disapprearing in the header 

{{ if .RSSLink }}
  <link href="https://cmsonline.cern.ch/Elog/htdocs/struts/feeds.jsp?sd=1311&msgUrl=https://cmsonline.cern.ch:443/webcenter/portal/cmsonline/pages_common/elog?Media-Type=screen&Media-Feature-Scan=0&Media-Feature-Orientation=landscape&Media-Feature-Device-Height=1080&Media-Feature-Height=960&_afrWindowMode=0&Media-Feature-Monochrome=0&Font-Size=16&Media-Feature-Color=8&Media-Featured-Grid=0&_afrLoop=2003489672914406&Media-Feature-Resolution=96&Media-Feature-Width=1853&Media-Feature-Device-Width=1920&Media-Feature-Color-Index=0&Adf-Window-Id=w0&__adfpwp_action_portlet=623564097&__adfpwp_backurl=https%3A%2F%2Fcmsonline.cern.ch%3A443%2Fwebcenter%2Fportal%2Fcmsonline%2Fpages_common%2Felog%3FMedia-Type%3Dscreen%26Media-Feature-Scan%3D0%26Media-Feature-Orientation%3Dlandscape%26Media-Feature-Device-Height%3D1080%26Media-Feature-Height%3D960%26_afrWindowMode%3D0%26Media-Feature-Monochrome%3D0%26Font-Size%3D16%26Media-Feature-Color%3D8%26Media-Featured-Grid%3D0%26_afrLoop%3D2003489672914406%26Media-Feature-Resolution%3D96%26Media-Feature-Width%3D1853%26Media-Feature-Device-Width%3D1920%26Media-Feature-Color-Index%3D0%26Adf-Window-Id%3Dw0%26__adfpwp_mode.623564097%3D1&_piref623564097.strutsAction=" rel="alternate" type="application/rss+xml" title="{{ .Site.Title }}" />
  <link href="{{ .RSSLink }}" rel="feed" type="application/rss+xml" title="{{ .Site.Title }}" />
{{ end }}

