---
title: "1108"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_1108.pdf](/images/Hamamatsu/6in_135ch/11XX/1108/IV_map_Hamamatsu_1108.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1108/IV_map_Hamamatsu_1108.png" caption="IV_map_Hamamatsu_1108.png" >}}

[IV_selCh_Hamamatsu_1108.pdf](/images/Hamamatsu/6in_135ch/11XX/1108/IV_selCh_Hamamatsu_1108.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1108/IV_selCh_Hamamatsu_1108.png" caption="IV_selCh_Hamamatsu_1108.png" >}}

[IV_selCh_Hamamatsu_1108_stats.pdf](/images/Hamamatsu/6in_135ch/11XX/1108/IV_selCh_Hamamatsu_1108_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1108/IV_selCh_Hamamatsu_1108_stats.png" caption="IV_selCh_Hamamatsu_1108_stats.png" >}}

</center>
