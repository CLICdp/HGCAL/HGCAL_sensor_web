---
title: "1109"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_1109.pdf](/images/Hamamatsu/6in_135ch/11XX/1109/IV_map_Hamamatsu_1109.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1109/IV_map_Hamamatsu_1109.png" caption="IV_map_Hamamatsu_1109.png" >}}

[IV_selCh_Hamamatsu_1109.pdf](/images/Hamamatsu/6in_135ch/11XX/1109/IV_selCh_Hamamatsu_1109.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1109/IV_selCh_Hamamatsu_1109.png" caption="IV_selCh_Hamamatsu_1109.png" >}}

[IV_selCh_Hamamatsu_1109_stats.pdf](/images/Hamamatsu/6in_135ch/11XX/1109/IV_selCh_Hamamatsu_1109_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1109/IV_selCh_Hamamatsu_1109_stats.png" caption="IV_selCh_Hamamatsu_1109_stats.png" >}}

</center>
