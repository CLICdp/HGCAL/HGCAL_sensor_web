---
title: "4005"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_4005.pdf](/images/Hamamatsu/6in_135ch/40XX/4005/IV_map_Hamamatsu_4005.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/40XX/4005/IV_map_Hamamatsu_4005.png" caption="IV_map_Hamamatsu_4005.png" >}}

[IV_selCh_Hamamatsu_4005.pdf](/images/Hamamatsu/6in_135ch/40XX/4005/IV_selCh_Hamamatsu_4005.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/40XX/4005/IV_selCh_Hamamatsu_4005.png" caption="IV_selCh_Hamamatsu_4005.png" >}}

[IV_selCh_Hamamatsu_4005_stats.pdf](/images/Hamamatsu/6in_135ch/40XX/4005/IV_selCh_Hamamatsu_4005_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/40XX/4005/IV_selCh_Hamamatsu_4005_stats.png" caption="IV_selCh_Hamamatsu_4005_stats.png" >}}

</center>
