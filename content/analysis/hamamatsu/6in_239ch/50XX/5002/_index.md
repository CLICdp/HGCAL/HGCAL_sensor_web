---
title: "5002"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_5002.pdf](/images/Hamamatsu/6in_239ch/50XX/5002/IV_map_Hamamatsu_5002.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/50XX/5002/IV_map_Hamamatsu_5002.png" caption="IV_map_Hamamatsu_5002.png" >}}

[IV_selCh_Hamamatsu_5002.pdf](/images/Hamamatsu/6in_239ch/50XX/5002/IV_selCh_Hamamatsu_5002.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/50XX/5002/IV_selCh_Hamamatsu_5002.png" caption="IV_selCh_Hamamatsu_5002.png" >}}

[IV_selCh_Hamamatsu_5002_stats.pdf](/images/Hamamatsu/6in_239ch/50XX/5002/IV_selCh_Hamamatsu_5002_stats.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/50XX/5002/IV_selCh_Hamamatsu_5002_stats.png" caption="IV_selCh_Hamamatsu_5002_stats.png" >}}

</center>
