---
title: "Z5471 No22"
date: 2019-04-25T15:55:26+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[Hamamatsu_Z5471-No22_IV_1000V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_1000V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_1000V.png" caption="Hamamatsu_Z5471-No22_IV_1000V.png" >}}

[Hamamatsu_Z5471-No22_IV_100V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_100V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_100V.png" caption="Hamamatsu_Z5471-No22_IV_100V.png" >}}

[Hamamatsu_Z5471-No22_IV_200V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_200V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_200V.png" caption="Hamamatsu_Z5471-No22_IV_200V.png" >}}

[Hamamatsu_Z5471-No22_IV_300V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_300V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_300V.png" caption="Hamamatsu_Z5471-No22_IV_300V.png" >}}

[Hamamatsu_Z5471-No22_IV_600V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_600V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_600V.png" caption="Hamamatsu_Z5471-No22_IV_600V.png" >}}

[Hamamatsu_Z5471-No22_IV_800V.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_800V.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_800V.png" caption="Hamamatsu_Z5471-No22_IV_800V.png" >}}

[Hamamatsu_Z5471-No22_IV_selCh.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_selCh.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_selCh.png" caption="Hamamatsu_Z5471-No22_IV_selCh.png" >}}

[Hamamatsu_Z5471-No22_IV_selCh_stats.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_selCh_stats.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No22/Hamamatsu_Z5471-No22_IV_selCh_stats.png" caption="Hamamatsu_Z5471-No22_IV_selCh_stats.png" >}}

</center>
