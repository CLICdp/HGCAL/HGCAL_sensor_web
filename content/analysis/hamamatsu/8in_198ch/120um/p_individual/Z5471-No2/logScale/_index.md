---
title: "LogScale"
date: 2019-04-25T15:55:26+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[Hamamatsu_Z5471-No2_IV_1000V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_1000V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_1000V_log.png" caption="Hamamatsu_Z5471-No2_IV_1000V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_100V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_100V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_100V_log.png" caption="Hamamatsu_Z5471-No2_IV_100V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_200V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_200V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_200V_log.png" caption="Hamamatsu_Z5471-No2_IV_200V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_300V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_300V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_300V_log.png" caption="Hamamatsu_Z5471-No2_IV_300V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_600V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_600V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_600V_log.png" caption="Hamamatsu_Z5471-No2_IV_600V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_800V_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_800V_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_800V_log.png" caption="Hamamatsu_Z5471-No2_IV_800V_log.png" >}}

[Hamamatsu_Z5471-No2_IV_selCh_log.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_selCh_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_selCh_log.png" caption="Hamamatsu_Z5471-No2_IV_selCh_log.png" >}}

[Hamamatsu_Z5471-No2_IV_selCh_log_stats.pdf](/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_selCh_log_stats.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/120um/Z5471-No2/logScale/Hamamatsu_Z5471-No2_IV_selCh_log_stats.png" caption="Hamamatsu_Z5471-No2_IV_selCh_log_stats.png" >}}

</center>
