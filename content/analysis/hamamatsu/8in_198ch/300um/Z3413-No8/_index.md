---
title: "Z3413 No8"
date: 2019-02-14T15:13:46+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[Hamamatsu_Z3413-No8_IV.pdf](/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV.png" caption="Hamamatsu_Z3413-No8_IV.png" >}}

[Hamamatsu_Z3413-No8_IV_selCh.pdf](/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV_selCh.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV_selCh.png" caption="Hamamatsu_Z3413-No8_IV_selCh.png" >}}

[Hamamatsu_Z3413-No8_IV_selCh_stats.pdf](/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV_selCh_stats.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3413-No8/Hamamatsu_Z3413-No8_IV_selCh_stats.png" caption="Hamamatsu_Z3413-No8_IV_selCh_stats.png" >}}

</center>
