---
title: "Z3413_5"
date: 2019-06-07T12:54:23+02:00
draft: false
weight: 5
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_200um_ind_Z3413_5_IV_-100V.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-100V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-100V.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_-100V.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_-200V.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-200V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-200V.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_-200V.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_-300V.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-300V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-300V.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_-300V.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_-500V.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-500V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-500V.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_-500V.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_-800V.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-800V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_-800V.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_-800V.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_5/HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh_stats.png" caption="HPK_8in_198ch_200um_ind_Z3413_5_IV_selCh_stats.png" >}}

</center>
