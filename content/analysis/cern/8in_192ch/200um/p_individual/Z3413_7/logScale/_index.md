---
title: "LogScale"
date: 2019-06-07T12:54:23+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/logScale/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/logScale/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log.png" caption="HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log_stats.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/logScale/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/logScale/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log_stats.png" caption="HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_log_stats.png" >}}

</center>
