---
title: "Z3413_7"
date: 2019-06-07T12:54:23+02:00
draft: false
weight: 7
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh.png" caption="HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh.png" >}}

[HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/200um/p_individual/Z3413_7/HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_stats.png" caption="HPK_8in_198ch_200um_ind_Z3413_7_IV_selCh_stats.png" >}}

</center>
