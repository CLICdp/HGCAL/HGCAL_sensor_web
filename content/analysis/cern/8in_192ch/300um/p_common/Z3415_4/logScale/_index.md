---
title: "LogScale"
date: 2019-06-07T12:54:22+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log_stats.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/logScale/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log_stats.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_log_stats.png" >}}

</center>
