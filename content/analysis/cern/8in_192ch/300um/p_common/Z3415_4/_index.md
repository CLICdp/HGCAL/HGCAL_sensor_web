---
title: "Z3415_4"
date: 2019-06-07T12:54:22+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-1000V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-100V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-200V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-300V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-500V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_-800V.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh.png" >}}

[HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_common/Z3415_4/HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_stats.png" caption="HPK_8in_198ch_300um_com_Z3415_4_IV_1stMeasurement_selCh_stats.png" >}}

</center>
