---
title: "LogScale"
date: 2019-06-07T10:53:35+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_120um_com_Z5471_19_IV_-100V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_-100V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_-100V_log.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_-100V_log.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_-200V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_-200V_log.pdf)

[HPK_8in_198ch_120um_com_Z5471_19_IV_-300V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_-300V_log.pdf)

[HPK_8in_198ch_120um_com_Z5471_19_IV_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_log.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_log.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log_stats.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/logScale/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log_stats.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_log_stats.png" >}}

</center>
