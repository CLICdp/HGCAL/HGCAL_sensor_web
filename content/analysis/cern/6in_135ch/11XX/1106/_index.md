---
title: "1106"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/11XX/1106

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_1106.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_map_1106.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_map_1106.png" caption="IV_map_1106.png" >}}

[IV_map_1106_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_map_1106_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_map_1106_details_all_heat.png" caption="IV_map_1106_details_all_heat.png" >}}

[IV_selCh_1106.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_selCh_1106.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_selCh_1106.png" caption="IV_selCh_1106.png" >}}

[IV_selCh_1106_stats.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_selCh_1106_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1106/IV_selCh_1106_stats.png" caption="IV_selCh_1106_stats.png" >}}

</center>
