---
title: "3006"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/30XX/3006

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3006.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_map_3006.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_map_3006.png" caption="IV_map_3006.png" >}}

[IV_map_3006_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_map_3006_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_map_3006_details_all_heat.png" caption="IV_map_3006_details_all_heat.png" >}}

[IV_selCh_3006.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_selCh_3006.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_selCh_3006.png" caption="IV_selCh_3006.png" >}}

[IV_selCh_3006_stats.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_selCh_3006_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3006/IV_selCh_3006_stats.png" caption="IV_selCh_3006_stats.png" >}}

</center>
