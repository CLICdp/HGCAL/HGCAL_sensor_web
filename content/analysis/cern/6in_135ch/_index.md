---
title: "6in_135ch"
date: 2018-08-15T14:06:34+02:00
draft: false
---

The following sensor comparison and plots are available:

{{%children %}}

The raw data can be found in:

{{%attachments style="orange" title="n-type" pattern="HPK_6in_135ch_11.*(txt)" /%}}

{{%attachments style="green" title="p-type common" pattern="HPK_6in_135ch_30.*(txt)" /%}}

{{%attachments style="blue" title="p-type individual" pattern="HPK_6in_135ch_40.*(txt)" /%}}
