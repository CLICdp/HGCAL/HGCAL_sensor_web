---
title: "3002"
date: 2018-08-15T14:06:33+02:00
draft: false
weight: 3002
---

EOS path: Analysis_IVmeas/8in_271ch/30XX/3002

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3002.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_map_3002.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_map_3002.png" caption="IV_map_3002.png" >}}

[IV_map_3002_details_all_heat.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_map_3002_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_map_3002_details_all_heat.png" caption="IV_map_3002_details_all_heat.png" >}}

[IV_selCh_3002.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_selCh_3002.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_selCh_3002.png" caption="IV_selCh_3002.png" >}}

[IV_selCh_3002_stats.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_selCh_3002_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3002/IV_selCh_3002_stats.png" caption="IV_selCh_3002_stats.png" >}}

</center>
