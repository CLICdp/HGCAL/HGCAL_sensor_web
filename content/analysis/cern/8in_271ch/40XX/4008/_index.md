---
title: "4008"
date: 2018-08-15T14:06:33+02:00
draft: false
---

EOS path: Analysis_IVmeas/8in_271ch/40XX/4008

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_4008.pdf](/images/Analysis_IVmeas/8in_271ch/40XX/4008/IV_map_4008.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/40XX/4008/IV_map_4008.png" caption="IV_map_4008.png" >}}

[IV_map_4008_details_all_heat.pdf](/images/Analysis_IVmeas/8in_271ch/40XX/4008/IV_map_4008_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/40XX/4008/IV_map_4008_details_all_heat.png" caption="IV_map_4008_details_all_heat.png" >}}

[all_HPK_8in_271ch_4008_IV_selChan.pdf](/images/Analysis_IVmeas/8in_271ch/40XX/4008/all_HPK_8in_271ch_4008_IV_selChan.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/40XX/4008/all_HPK_8in_271ch_4008_IV_selChan.png" caption="all_HPK_8in_271ch_4008_IV_selChan.png" >}}

[all_HPK_8in_271ch_4008_IV_selChan_stats.pdf](/images/Analysis_IVmeas/8in_271ch/40XX/4008/all_HPK_8in_271ch_4008_IV_selChan_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/40XX/4008/all_HPK_8in_271ch_4008_IV_selChan_stats.png" caption="all_HPK_8in_271ch_4008_IV_selChan_stats.png" >}}

</center>
