---
title: "LogScale"
date: 2019-06-25T11:55:12+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_6in_239ch_120um_pcom_5006_IV_-1000V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-1000V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-1000V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-1000V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_-100V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-100V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-100V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-100V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_-200V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-200V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-200V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-200V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_-300V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-300V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-300V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-300V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_-500V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-500V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-500V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-500V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_-800V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-800V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_-800V.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_-800V.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_selCh.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_selCh.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_selCh.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_selCh_stats.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_selCh_stats.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_selCh_stats.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_totcurr.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_totcurr.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_totcurr.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_totcurr.png" >}}

[HPK_6in_239ch_120um_pcom_5006_IV_totcurr_stats.pdf](/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_totcurr_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/50XX/5006/logScale/HPK_6in_239ch_120um_pcom_5006_IV_totcurr_stats.png" caption="HPK_6in_239ch_120um_pcom_5006_IV_totcurr_stats.png" >}}

</center>
