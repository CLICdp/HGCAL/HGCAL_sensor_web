---
title: "60XX"
date: 2019-06-25T11:55:13+02:00
weight: 3
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[comparison_log_means_summary.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_log_means_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_log_means_summary.png" caption="comparison_log_means_summary.png" >}}

[comparison_log_medians_summary.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_log_medians_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_log_medians_summary.png" caption="comparison_log_medians_summary.png" >}}

[comparison_means_summary.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_means_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_means_summary.png" caption="comparison_means_summary.png" >}}

[comparison_medians_summary.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_medians_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_medians_summary.png" caption="comparison_medians_summary.png" >}}

[comparison_totcurr.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_totcurr.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_totcurr.png" caption="comparison_totcurr.png" >}}

[comparison_totcurr_log.pdf](/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_totcurr_log.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/60XX/comparison_totcurr_log.png" caption="comparison_totcurr_log.png" >}}

</center>
