---
title: "Clean Room Monitor"
description: Web page dedicated to monitor the clean room
weight: 1000
---

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

This page collects automatically produced plots to monitor the clean room and the freezer for the last 24 hours and 15 days.

Legend and more info at the end of the page.

<center>
## Clean room monitor
{{< img src="/images/monitor/biweekly_plot.png" caption="Monitor last 2 weeks" >}}
{{< img src="/images/monitor/today_plot.png" caption="Daily monitoring" >}}

## Freezer monitor
{{< img src="/images/monitor_freezer/biweekly_plot.png" caption="Monitor last 2 weeks" >}}
{{< img src="/images/monitor_freezer/today_plot.png" caption="Daily monitoring" >}}
</center>

Legend in #particles > 0.5 um / inch^3:

* <span style="color:purple">Purple line</span>: Class ISO 5 (FED STD 209E Class 100)

* <span style="color:green">Green line</span>: Class ISO 6 (FED STD 209E Class 1'000)

* <span style="color:blue">Blue line</span>: Class ISO 7 (FED STD 209E Class 10'000)

* <span style="color:red">Red line</span>: Class ISO 8 (FED STD 209E Class 100'000)

The cleanroom classification is taken from [Wikipedia](https://en.wikipedia.org/wiki/Cleanroom).

The cleanroom monitoring is performed by a raspeberry-pi (pchglog01) with a specific
 [software](https://github.com/twentycrooks/clean_room_monitor.git).



