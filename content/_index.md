---
title: "About"
description: Description of HexWeb purpose and structure
---

# HexWeb

## Content of this web page

Here you can find:

* [Geometries]({{%relref "geometries/_index.md" %}}): This page collects the geometry of the HGCal sensors analyzed in the lab including the cell numbering scheme.

* [Sensors analysis]({{%relref "analysis/_index.md" %}}): In this page the analysis done on different HGCal sensor batches are reported.
Description of the analysis method is also included, if necessary.

* [Sensors list]({{%relref "sensorslist/_index.md" %}}): All default plots produced by HexPlot and HexDAQ for each sensor analyzed are displayed here.

* [Clean Room Monitor]({{%relref "monitor/_index.md" %}}): Temperature, pressure, humidity and number of particles in the clean room for the last 24 hours and 15 days are reported in this web page.

* [Posts]({{%relref "posts/_index.md" %}}): The lab logbook is here reported.

{{% notice warning %}}
The posts are not included yet.
{{% /notice %}}
